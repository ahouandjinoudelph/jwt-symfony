<?php


namespace App\Exceptions;


use Exception;
use Symfony\Component\HttpFoundation\Response;

class NoContentException extends Exception implements ApiExceptionInterface
{
    public function __construct($message = null)
    {
        parent::__construct($message, Response::HTTP_NO_CONTENT);
    }
}
