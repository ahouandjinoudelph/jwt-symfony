<?php

namespace App\Controller;

use App\Entity\User;
use App\Exceptions\ConflictException;
use App\Exceptions\NoContentException;
use App\Exceptions\UnprocessableException;
use App\Payload\ApiResponse;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Serializer\SerializerInterface;

class AuthApiController extends AbstractController
{

    /**
     * @param Request $request
     * @param UserPasswordEncoderInterface $encoder
     * @param SerializerInterface $serializer
     * @param UserRepository $userRepository
     * @return JsonResponse
     * @throws ConflictException
     * @throws UnprocessableException
     */
    public function register(SerializerInterface $serializer, Request $request, UserRepository $userRepository, UserPasswordEncoderInterface $encoder)
    {
        $em = $this->getDoctrine()->getManager();
        $newUser = $serializer->deserialize($request->getContent(), User::class, 'json');

        if (!$newUser->getUsername() ||  !$newUser->getSurname() || !$newUser->getEmail() || !$newUser->getPassword() || !$newUser->getName()) {

            throw new UnprocessableException("One or more value is blank");
        }


        $email   = $userRepository->findOneBy(['email' => $newUser->getEmail()]);
        $surname = $userRepository->findOneBy(['surname' => $newUser->getSurname()]);
        $name    = $userRepository->findOneBy(['name' => $newUser->getName()]);
        $username = $userRepository->findOneBy(['username' => $newUser->getUsername()]);
        var_dump($email, $surname, $name, $username);

        if ($email) {
            throw new ConflictException($email->getEmail() . " already exist");

        }
        if ($surname) {
            throw new ConflictException($surname->getSurname() . " already exist");
        }
        if ($name) {
            throw new ConflictException($name->getName() . " already exist");
        }
        if ($username) {
            throw new ConflictException($username->getUsername() . " already exist");
        }

        $newUser->setPassword($encoder->encodePassword($newUser, $newUser->getPassword()));
        $em->persist($newUser);
        $em->flush();


        return $this->json(new ApiResponse(true, "Done"), 200, [], []);
    }

    /**
     * @param UserInterface $user
     * @param JWTTokenManagerInterface $JWTManager
     * @return JsonResponse
     */
    public function getTokenUser(UserInterface $user, JWTTokenManagerInterface $JWTManager)
    {

        return new JsonResponse(['token' => $JWTManager->create($user)]);
    }




}
